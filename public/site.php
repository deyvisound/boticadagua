
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<?php include VIEWS."site/header.php"; ?>
	</head>
	<body>

		<!--Banner-->
		<?php include VIEWS."site/banner.php"; ?>	
		<!--/Banner-->

		<!--About-->
		<?php include VIEWS."site/about.php"; ?>	
		<!--/About-->

		<!--services-->
		<?php include VIEWS."site/services.php"; ?>	
		<!--/services-->

		<!--team-->
		<?php include VIEWS."site/team.php"; ?>	
		<!--/team-->

		<!--Pricing-->
		<?php include VIEWS."site/pricing.php"; ?>	
		<!--/Pricing-->

		<!--Subscribe-->
		<?php include VIEWS."site/subscribe.php"; ?>	
		<!--/Subscribe-->

		<!--Página de Contato-->
		<?php include VIEWS."site/contact.php"; ?>	
		<!--/Página de Contato-->

		<!--Rodapé-->
		<?php include VIEWS."site/footer.php"; ?>
		<!--/Rodapé-->

		<!--Avisos-->
		<?php include VIEWS."mesages/maintenance.php"; ?>

	</body>
</html>
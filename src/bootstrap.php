<?php

/**
* Site boticadagua com slim framework 2
*
***/

require RAIZ.'vendor/autoload.php';
require RAIZ.'src/controllers/UtilsController.php';

// Instanciando a applicação Slim
$app = new \Slim\Slim();

// Instância de utilitários
$utils = new UtilsController($app);

//constantes
define("VIEWS", RAIZ."src/views/");
define("ASSETS", $utils->getRootUri()."/assets/");


//Rotas do sistema
include __DIR__."/routes.php";




//Executando aplicação
$app->run();



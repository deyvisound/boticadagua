<!-- pricing -->
<div id="pricing" class="pricing">
	<div class="container">
		<div class="tittle-agileinfo">
			<h3 class="w3_head w3_head1">Benefícios da Água Purificada</h3>
			<p class="toppara">Beber dois litros de água por dia faz bem à saúde e à beleza. A água regula as funções do organismo, evita cáculos renais e controla a saciedade, entre outros benefícios.</p>
		</div>
		<div class="sreen-gallery-cursual">
			
			<div id="owl-demo" class="owl-carousel">
				<div class="item-owl">
					<div class="test-review">						
						<h5>Poderosa arma contra a celulite</h5>
						<p class="innerpara">Ajuda o organismo a eliminar as impurezas, além de facilitar a evacuação e melhorar a circulação sanguínea consequentemente evitando o aparecimento da celulite.</p>						
					</div>
				</div>
				<div class="item-owl">
					<div class="test-review">
						<h5>Hidrata o corpo</h5>
						<p class="innerpara">É essencial manter o corpo bem hidratado, de preferência com água livre de bactérias e impurezas.</p>
					</div>
				</div>
				<div class="item-owl">
					<div class="test-review">						
						<h5>Unhas e cabelos hidratados</h5>
						<p class="innerpara">Quando nos hidratamos corretamente, fica notório principalmente nas unhas e cabelo.</p>
					</div>
				</div>
				<div class="item-owl">
					<div class="test-review">						
						<h5>Combate o envelhecimento</h5>
						<p class="innerpara">Enquanto os cremes e hidratantes agem de fora pra dentro, a água é capaz de hidratar as camadas mais profundas, agindo de dentro pra fora.</p>			
					</div>
				</div>
				<div class="item-owl">
					<div class="test-review">						
						<h5>Ajuda a emagrecer</h5>
						<p class="innerpara">Ao bebermos água antes e durante as refeições 
						aumentamos a sensação de saciedade, além de melhorar o processo digestivo.</p>			
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!--pricing-->
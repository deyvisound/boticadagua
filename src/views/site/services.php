<!-- services -->
<div class="services" id="services">
	<div class="container">
		<h3 class="w3_head w3_head1">Assistência Técnica</h3>
		<p class="toppara">Para solicitar os serviços de assistência técnica, ligue:
        <br> <strong>(19) 3228-3301 | 3273-6477 | 0800 774 1223 </strong></p>
	</div>
	<div class="agileits_services_grids_bottom">
		<div class="container">
			<div class="col-md-4 agileits_service_grid_btm_left">
				<div class="agileits_service_grid_btm_left1">
					<img src="<?php echo ASSETS; ?>images/4.jpg" alt=" " class="img-responsive" />
					<div class="agileits_service_grid_btm_left2">
						<h5>Atendimento<span class="fa fa-suitcase"></span></h5>
						<p> Dispomos de um excelente atendimento etc etc
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 agileits_service_grid_btm_left">
				<div class="agileits_service_grid_btm_left1">
					<img src="<?php echo ASSETS; ?>images/5.jpg" alt=" " class="img-responsive" />
					<div class="agileits_service_grid_btm_left2">
						<h5>Equipe Técnica <span class="fa fa-building"></span></h5>
						<p>A assistência técnica é formada por excelentes profissionais etc etc</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 agileits_service_grid_btm_left">
				<div class="agileits_service_grid_btm_left1">
					<img src="<?php echo ASSETS; ?>images/6.jpg" alt=" " class="img-responsive" />
					<div class="agileits_service_grid_btm_left2">
						<h5>Tecnologia<span class="fa fa-cubes"></span></h5>
						<p>Fábricas equipadas com equipamentos de última geração... etc...</p>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //services -->
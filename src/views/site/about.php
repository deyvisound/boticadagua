<!-- about -->
<div class="about" id="about">
	<div class="container">
		<h3 class="w3_head w3_head1">Purificadores de alta performance</h3>
		<p class="toppara">
			Nossos purificadores possuem eficiência bacteriológica aprovada, ou seja, 
			sua água ficará livre de bactérias garantindo assim uma água pura de verdade 
			para toda sua família.
		</p>
		<div class="aboutgrids">
			<div class="col-md-6 aboutleft">
				<h6>QUALIDADE</h6>
				<h4>Nossos purificadores foram aprovados pelo <b>Inmetro</b> de acordo com a portaria 344/2014</h4>				
				<p>São destacados como os melhores do Em:</p>
				<div class="about-inner">
					<div class="about-inner-top">
						<div class="col-md-2">
							<i class="fa fa-database" aria-hidden="true"></i>
						</div>
						<div class="col-md-10">
							<h4>Economia de Energia</h4>
							<p>Curabitur id urna sit amet tellus cursus consectetur 
							a nec nisi. Morbiimpe rdiet gravida dolor Nunc accumsan</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="about-inner-bottom">
						<div class="col-md-10">
							<h4>Capacidade de fornecimento de água gelada</h4>
							<p>Curabitur id urna sit amet tellus cursus consectetur 
							a nec nisi. Morbiimpe rdiet gravida dolor Nunc accumsan</p>
						</div>
						<div class="col-md-2">
							<i class="fa fa-database" aria-hidden="true"></i>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 aboutright">
				<img src="<?php echo ASSETS; ?>images/about.jpg" alt="aboutimage" style="border-radius: 1em;"/>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //about -->
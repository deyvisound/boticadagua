<!-- coding skills -->
<div class="w3ls_agile_circle_progress agile_info_shadow" id="skills">
	<div class="container">
		<div class="cir_agile_info ">
			<h3 class="w3_head w3_head1">Our Skills </h3>
			<p class="toppara">Etiam molestie leo ac magna sodales, a tincidunt nibh egestas. 
				Praesent mattis luctus sapien. Nunc accumsan quis tortor et commodo
			Proin nec rhoncus orci. a tincidunt nibh egestas</p>
			<div class="skill-grids">
				<div class="skills-grid text-center">
					<div class="circle" id="circles-1"></div>
					<h4>Design Management</h4>
				</div>
				<div class="skills-grid text-center">
					<div class="circle" id="circles-2"></div>
					<h4>Web Development</h4>
				</div>
				<div class="skills-grid text-center">
					<div class="circle" id="circles-3"></div>
					
					<h4>Mobile Application</h4>
				</div>
				<div class="skills-grid text-center">
					<div class="circle" id="circles-4"></div>
					<h4>Technology Skills</h4>
				</div>
				
				
				
				<div class="clearfix"></div>
				
			</div>
		</div>
	</div>
</div>
<!-- //coding skills --> 
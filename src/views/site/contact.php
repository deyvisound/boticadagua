<!-- contact -->
<div id="contact" class="contact">
	<div class="container">
		
		<div class="tittle-agileinfo">
			<h3 class="w3_head w3_head1">Fale Conosco</h3>
			<p class="toppara">
				A <b>Botica D'agua</b> dispõe de vendedores e antendes 
				em horário comercial para melhor atendê-lo.<br/>
				Visite nossa loja!
			</p>
		</div>
		
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3969.337143547291!2d-35.210820550439955!3d-5.807971558881763!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b2fffc3ac89511%3A0x23b65d108cf3016e!2sAv.+Prudente+de+Morais%2C+2913+-+Candel%C3%A1ria%2C+Natal+-+RN%2C+59022-000!5e0!3m2!1sen!2sbr!4v1530052529136" frameborder="0" style="border:0" allowfullscreen></iframe>

		<div class="contactimain">
			<div class="col-md-6 contact-left">
				<form action="#" method="post">
					<div class="wthree_contact_left_grid">
						<input type="text" name="Name" placeholder="Name" required="">
						<input type="email" name="Email" placeholder="Email" required="">
						<input type="text" name="Telephone" placeholder="Telephone" required="">
						<input type="text" name="Subject" placeholder="Subject" required="">
						<textarea name="Message" placeholder="Message..." required=""></textarea>
						<input type="submit" value="Submit">
					</div>
					<div class="clearfix"> </div>
				</form>
			</div>
			<div class="col-md-6 contact-right">
				<div class="agile_info_mail_img_info">
					<div class="address-grid">
						<h4>Informações de Contato</h4>						
						<div class="mail-agileits-w3layouts">
							<div class="contacticon">
								<i class="fa fa-volume-control-phone" aria-hidden="true"></i>
							</div>
							<div class="contact-right-inner">
								<p>Telefone </p><span><a href="#">(84)3345-2759</a></span>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="mail-agileits-w3layouts">
							<div class="contacticon">
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
							</div>
							<div class="contact-right-inner">
								<p>E-Mails</p>
								<a href="mailto:atendimento@boticadagua.com.br">atendimento@boticadagua.com.br</a><br/>
								<a href="mailto:vendas@boticadagua.com.br">vendas@boticadagua.com.br</a>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="mail-agileits-w3layouts">							
							<div class="contact-right-inner">
								<p class="contacttext"><br/>
									<b style="color: #000;">Loja Natal/RN:</b><br/>
									Av. Prudente de Morais, 2913 - Lagoa Seca - CEP 59.022-310
								</p>
								<br/>
								<p class="contacttext">
									<b style="color: #000;">Loja Recife/PE:</b><br/>
									Rua Arão Lins de andrade 848 - Jaboatão dos guararapes 
								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //contact -->
<!-- banner -->
<div id="home" class="main_agileits">
	<div class="agile_wthree_nav">
		<nav class="navbar navbar-default">
			<div class="navbar-header navbar-left">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<h1>
					<a class="navbar-brand" href="#home">
						<img id="logo_principal" src="<?php echo ASSETS; ?>images/logo/logo_wbg.png"/>
					</a>
				</h1>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->

			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
				<nav class="link-effect-8" id="link-effect-8">
					<ul class="nav navbar-nav">
						<li class=" menu-li active"><a href="#home" class="scroll">Início</a></li>
						<li class=" menu-li"><a href="#about" class="scroll">Sobre</a></li>
						<li class=" menu-li"><a href="#services" class="scroll">Assistência</a></li>
						<li class=" menu-li"><a href="#team" class="scroll">Produtos</a></li>
						<li class=" menu-li" ><a href="#contact" class="scroll">Contato</a></li>
					</ul>
				</nav>
			</div>			
		</nav>

	</div>

	<div class="s1">
		<h3>Purificadores de alta performance</h3>		
		<div class="w3-button">
			<div class="w3ls-button">
				<a href="#" class="hvr-shutter-out-vertical">Saiba Mais</a>
			</div>
			<div class="w3l-button">
				<a href="#contact" class="hvr-shutter-out-vertical scroll">Entre em Contato</a>
			</div>
			<div class="clearfix"> </div>
		</div>

	</div>

	<div class="slides-box">
		<ul class="slides">
			<li style="background: url(<?php echo ASSETS; ?>images/banner/banner1.png)center"></li>
			<li style="background: url(<?php echo ASSETS; ?>images/banner/banner2.jpg) center"></li>	
			<li style="background: url(<?php echo ASSETS; ?>images/banner/banner3.jpg) center"></li>
			<li style="background: url(<?php echo ASSETS; ?>images/banner/banner4.jpg)center"></li>
		</ul>
	</div>
</div>
	<!-- /banner -->

<?php

/**
* Cabeçalho
*/

?>

<title>Botica D´agua</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="keywords" content="Água, Filtros, purificadores, cloro, ozônio, tratamento de Água, Água tratada"/>

<script type="application/x-javascript"> 
	addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
	function hideURLbar(){ window.scrollTo(0,1); } 
</script>	

<link href="<?php echo ASSETS; ?>css/circles.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo ASSETS; ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo ASSETS; ?>css/font-awesome.css" rel="stylesheet"> 
<link href="<?php echo ASSETS; ?>css/style.css" rel="stylesheet" type="text/css" media="all" />
	
<link href="<?php echo ASSETS; ?>css/poposlides.css" rel="stylesheet" >

<link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
<link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
<!--//fonts-->

<link href="<?php echo ASSETS; ?>css/myStyle.css" rel="stylesheet" type="text/css" media="all" />
<!-- team -->
<div class="team" id="team">
	<div class="container">
		<h3 class="w3_head w3_head1">Linha de Produtos</h3>
		<p class="toppara">
			Produtos duráveis, resistentes e econômicos. Os purificadores refrigerados se destacam por seu eficiente sistema de refrigeração por compressor, com capacidade de fornecimento de água gelada de 2.4 litros/hora. Com acabamento em aço carbono, garante maior durabilidade. Além de manter a água gelada por mais tempo, garante qualidade e pureza sem deixar gosto e cheiro.
		</p>
		<div class="team-grids">
			<div class="teamtopgrids">
				<?php

				$purificadores = array(
					"HEALTH ENERGY PURIFICADOR ALCALINO",
					"HEALTH ENERGY COM SUPER OZÔNIO",
					"NEWOXI PURIFICADOR COM SUPER OZÔNIO",
					"NEW PLATINUM",
					"PLATINUM FLEX",
					"PLATINUM COMPACT",
					"PLUS HEALTH ENERGY",
					"PLUS LIFE",
					"BLUE LIFE",
					"GOLD LINE",
					"SILVER LIFE",
					"BEBEDOURO INDIVIDUAL",
					"BEBEDOURO CONJUGADO",
					"REFIL INDIVIDUAL",
					"REFIL DUPLO",
					"REFIL ALCALINO"
				);

				?>

				<?php 
					$i = 0; 
					foreach($purificadores as $purificador): 						
				?>
					<div class="col-md-3">
						<div class="teamgrid1">
							<img src="<?php echo ASSETS; ?>images/produtos/image-0000<?php echo str_pad($i, 2, "00", STR_PAD_LEFT); ?>.png" alt="team1" />
							<div class="p-mask">
								<h5 class="ng-binding"><?php echo $purificador; ?></h5>
							</div>
							<!--
							<div class="teaminfo">
								<p><?php //echo mb_strtoupper($purificador); ?></p>
							</div>
							-->
						</div>
					</div>
				<?php 
						$i++;
				 	endforeach; 
				?>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	
</div>
<!-- team -->
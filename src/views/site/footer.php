
<!-- Footer -->
<div class="footer w3ls">
	<div class="container">
		<div class="ftr-menu">
			<ul>
				<li><a href="#home" class="scroll">Início</a></li>
				<li><a href="#about" class="scroll">Sobre</a></li>
				<li><a href="#services" class="scroll">Assistência</a></li>
				<li><a href="#team" class="scroll">Produtos</a></li>
				<li><a href="#pricing" class="scroll">Benefícios</a></li>
				<li><a href="#contact" class="scroll">Contato</a></li>
			</ul>
		</div>
		<div class="w3ls-social-icons-2">
			<a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
			<a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
			<a class="pinterest" href="#"><i class="fa fa-google-plus"></i></a>
			<a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
			<a class="tumblr" href="#"><i class="fa fa-tumblr"></i></a>
		</div>
		<div class="logo-fo">
			<h2><a href="#home">BOTICA D'AGUA</a></h2>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
<div class="copyrights">
	<p>&copy; 2018 Modern. All rights reserved | Design by 		
		<a href="https://deyvisound.github.io/portfolio" target="blank">Deyvison Estevam</a> and 
		<a href="http://w3layouts.com" target="blank">W3layouts</a>
	</p>
</div>
<!-- //Footer -->




<!-- js -->
<script type="text/javascript" src="<?php echo ASSETS; ?>js/jquery-2.1.4.min.js"></script>

<!-- banner js -->
<script src="<?php echo ASSETS; ?>js/search.js"></script>
<script src="<?php echo ASSETS; ?>js/poposlides.js">
</script>
<script>
	$(".slides").poposlides();
</script>
<!-- //banner js -->

<!--pop-up-grid-->
<div id="popup">
	<div id="small-dialog" class="mfp-hide">
		<div class="signin-form profile">
			<h3>Sign Up</h3>

			<div class="login-form">
				<form action="#" method="post">
					<input type="text" name="name" placeholder="Name" required="">
					<input type="email" name="email" placeholder="E-mail" required="">
					<input type="text" name="phone" placeholder="Phone number" required="">
					<input type="password" name="password" placeholder="Password" required="">
					<input type="password" name="password" placeholder="Confirm Password" required="">
					<input type="submit" value="Sign Up">
				</form>
			</div>
		</div>
	</div>
</div>		


<!--pop-up-grid-->
<script src="<?php echo ASSETS; ?>js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

	});
</script>


<!-- required-js-files-->
<link href="<?php echo ASSETS; ?>css/owl.carousel.css" rel="stylesheet">
<script src="<?php echo ASSETS; ?>js/owl.carousel.js"></script>
<script>
	$(document).ready(function() {
		$("#owl-demo").owlCarousel({
			items :3,
			itemsDesktop : [768,2],
			itemsDesktopSmall : [414,1],
			lazyLoad : true,
			autoPlay : true,
			navigation :true,

			navigationText :  false,
			pagination : true,

		});

	});
</script>
<!--//required-js-files-->


<!-- /circle-->
<script type="text/javascript" src="<?php echo ASSETS; ?>js/circles.js"></script>
<script>
	var colors = [
	['none', '#fd9426'], ['#fafafa', '#fc3158'],['#fafafa', '#53d769'], ['#fafafa', '#147efb']
	];
	for (var i = 1; i <= 7; i++) {
		var child = document.getElementById('circles-' + i),
		percentage = 30 + (i * 10);

		Circles.create({
			id:         child.id,
			percentage: percentage,
			radius:     80,
			width:      13,
			number:   	percentage / 1,
			text:       '%',
			colors:     colors[i - 1]
		});
	}

</script>
<!-- //circle -->

<script src="<?php echo ASSETS; ?>js/SmoothScroll.min.js"></script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?php echo ASSETS; ?>js/move-top.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
	$(document).ready(function() {
	/*
		var defaults = {
		containerID: 'toTop', // fading element id
		containerHoverID: 'toTopHover', // fading element hover id
		scrollSpeed: 1200,
		easingType: 'linear' 
		};
		*/

		$().UItoTop({ easingType: 'easeOutQuart' });

	});
</script>

<!-- move to top-js-files -->
<script type="text/javascript" src="<?php echo ASSETS; ?>js/move-top.js"></script>
<script type="text/javascript" src="<?php echo ASSETS; ?>js/easing.js"></script>
<!-- //move to top-js-files -->


<script type="text/javascript" src="<?php echo ASSETS; ?>js/bootstrap.js"></script><!-- bootstrap js file -->

<!-- My script-->

<script type="text/javascript">
	$( ".menu-li" ).each(function( index ) {
		$( this ).removeClass( "active" );
	});

	$( this ).addClass( "active" );
</script>
<!-- subscribe -->
<div class="subscribe">
	<div class="container">
		<div class="tittle-agileinfo">
			<h3 class="w3_head w3_head1">Fique por Dentro</h3>
			<p class="toppara">
				Cadastre seu email para receber dicas e novidades sobre o que há de mais novo no mercado 
				de filtros e purificadores. Conheça melhor nossa tecnologia e matenha sua saúde em dia. 
			</p>
		</div>
		<div class="w3-agile-subscribe">
			<form action="#" method="post">
				<input type="email" id="mc4wp_email" name="EMAIL" placeholder="Enter your email here" required="">
				<input type="submit" value="Inscrever-se">
			</form>
			<h4>OU</h4>
			<p class="toppara">
				Sinta-se a vontade para nos seguir em nossas redes sociais.
			</p>
			<div class="agileinfo-social-grids">
				<ul>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-rss"></i></a></li>
					<li><a href="#"><i class="fa fa-vk"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- //subscribe -->
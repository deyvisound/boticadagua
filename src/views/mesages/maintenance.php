<!-- Modal -->
<div id="banner_aviso_container" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <img id="banner_aviso" src="<?php echo ASSETS; ?>images/maintenance.jpg"/>
    </div>

  </div>
</div>

<button id="buttonOpenBannerAvisoContainer" type="button" data-toggle="modal" data-target="#banner_aviso_container" style="display: none;"></button>

<script>
  document.addEventListener("DOMContentLoaded", function(){    
    document.getElementById("buttonOpenBannerAvisoContainer").click();
  });
</script>
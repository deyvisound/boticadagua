<?php

/**
*
* Class com alguns utilitários
*
*/


class UtilsController{
	
	private $slimApp = null;

	function __construct(\Slim\Slim $app){
		$this->slimApp = $app;
	}


	public function getRootUri(){
		// Get request object
		$req = $this->slimApp->request;

		//Get root URI
		return $req->getRootUri();
	}

	/**
	*/
	public function getResourceUri(){
		// Get request object
		$req = $this->slimApp->request;

		//Get resource URI
		return $req->getResourceUri();
	}
}